# angular-pagination

Bienvenido al repositorio de angular-pagination, una aplicación Angular desarrollada por Josan Granado, especialista en tecnologías web con amplia experiencia en JavaScript, Java, y sus frameworks asociados como Angular, Ionic, Loopback 4, Nest JS, y Spring.

## Descripción

angular-pagination es una solución web dinámica que presenta una lista paginada de usuarios, permitiendo a los usuarios filtrar la búsqueda por nombre, apellido y correo electrónico. Esta aplicación es ideal para administradores de sistemas, gestores de usuarios o cualquier interesado en visualizar y gestionar usuarios de manera eficiente.

### Tecnologías Utilizadas

- **Frontend:** Angular 17.1.2, aprovechando las últimas características y mejores prácticas del framework.
- **API Virtual:** Utilizamos `json-server` para simular una API de backend, facilitando el desarrollo y pruebas con datos de usuarios reales.

## Características Principales

- **Lista de Usuarios Paginada:** Navega a través de una lista extensa de usuarios con facilidad.
- **Filtrado Dinámico:** Encuentra rápidamente usuarios específicos utilizando filtros por nombre, apellidos y email.
- **Interfaz Amigable:** Diseñada para ofrecer una experiencia de usuario intuitiva y agradable.

## Inicio Rápido

Para ejecutar angular-pagination en tu entorno de desarrollo local, sigue estos pasos:

1. **Pre-requisitos:** Asegúrate de tener instalado Node.js y Angular CLI en tu sistema.

2. **Instalación de Dependencias:**

   ```console
   npm install
3. **Arrancar la Aplicación:** Utiliza el comando personalizado para iniciar tanto la API virtual como la aplicación Angular:

   ```console
   npm run start:dev
## Desarrollo y Contribuciones
Si estás interesado en contribuir a angular-pagination, considera lo siguiente:

* **Sugerencias y Mejoras:** Abre un issue para discutir cambios potenciales o mejoras.
* **Pull Requests:** Son bienvenidos para correcciones de errores, mejoras o nuevas funcionalidades.

## Soporte
Para obtener ayuda adicional con angular-pagination, puedes contactarme directamente o abrir un issue en este repositorio.
