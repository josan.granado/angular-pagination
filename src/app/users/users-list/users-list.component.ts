import { Component, OnChanges, OnInit } from '@angular/core';
import { UsersService } from '../../services/users.service';
import { User } from '../../services/models/user.model';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrl: './users-list.component.scss',
})
export class UsersListComponent implements OnInit {
  users: User[] = [];

  // Parámetros de paginación
  actualPage = 1;
  pageSize = 10;
  totalPages = 0;

  // Filtros de búsqueda
  filterName = '';
  filterSurname = '';
  filterEmail = '';

  // Datos de paginación
  totalResult = 0;
  firstResult = 0;
  lastResult = 0;
  pagesList: number[] = [];

  constructor(public usersService: UsersService) {}

  ngOnInit() {
    // Cargamos los datos de los usuarios
    this.getUsers();
  }

  /**
   * Esta funcion se encarga de obtener los datos de todos los usuarios
   */
  getUsers() {
    // Cargamos todos los usuarios de la API
    this.usersService.getUsers().subscribe({
      next: (data) => {
        this.totalResult = data.length;
        this.users = data.map((user: User) => ({
          id: user.id,
          name: user.name,
          email: user.email,
          surname1: user.surname1,
          surname2: user.surname2,
        } as User));
        // Filtramos los resultados de la carga de usuarios
        this.filterAndPaginateUsers();
      },
      error: (error) => {
        console.error('Ha ocurrido un error!', error);
      },
    });
  }

  /**
   * Esta funcion se encarga de filtrar y paginar los usuarios
   */
  filterAndPaginateUsers() {
    let filteredUsers = this.users.filter((user) => {
      return (
        user.name.toLowerCase().includes(this.filterName.toLowerCase()) &&
        user.surname1.toLowerCase().includes(this.filterSurname.toLowerCase()) &&
        user.email.toLowerCase().includes(this.filterEmail.toLowerCase())
      );
    });

    this.users = filteredUsers.slice(
      (this.actualPage - 1) * this.pageSize,
      this.actualPage * this.pageSize
    );

    this.firstResult = (this.actualPage - 1) * this.pageSize + 1;
    this.totalResult = filteredUsers.length;
    this.lastResult = this.actualPage * this.pageSize;
    this.totalPages = Math.ceil(filteredUsers.length / this.pageSize);
    this.getPagesList();
  }

  getPagesList() {
    this.pagesList = [];
    for (let i = 1; i <= this.totalPages; i++) {
      this.pagesList.push(i);
    }
  }

  onParamsChange(actualPage?: number) {
    this.actualPage = actualPage || 1;
    this.getUsers();
  }

  nextPage() {
    if (this.actualPage < this.totalPages) {
      this.actualPage++;
      this.onParamsChange(this.actualPage);
    }
  }

  previousPage() {
    if (this.actualPage > 1) {
      this.actualPage--;
      this.onParamsChange(this.actualPage);
    }
  }
}
